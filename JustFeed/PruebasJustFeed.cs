﻿using System;
using System.Collections.Generic;

namespace JustFeed
{
    public class Pedido
    {
        public string NombreCliente { get; set; }
        public string DireccionEntrega { get; set; }
        public string[] PlatosAPedir { get; set; }
        public Pedido(string nombre, string direccion, string[] platos)
        {
            this.NombreCliente = nombre;
            this.DireccionEntrega = direccion;
            this.PlatosAPedir = platos;
        }
    }
    public class ColaReparto : Queue<Pedido>
    {
        private Queue<Pedido> colaPedido;
        public ColaReparto(){ colaPedido = new Queue<Pedido>(); }
        public void AgregarPedido(string nombre, string direccion, string[] platos)
        {
            Pedido pedido = new Pedido(nombre, direccion, platos);
            colaPedido.Enqueue(pedido);
        }
        public void MostrarProximoPedido()
        {
            Pedido Pedido = (Pedido)colaPedido.Peek();
            Console.WriteLine(Pedido.NombreCliente + ", " + Pedido.DireccionEntrega);

        }

        public void EliminarProximoPedido()
        {
            Pedido Pedido = (Pedido)colaPedido.Peek();
            Console.WriteLine(Pedido.NombreCliente + ", " + Pedido.DireccionEntrega + " atendida");
            colaPedido.Dequeue();
        }
    }
    class PruebasJustFeed
    {
        static void Main(string[] args)
        {
            ColaReparto repartos = new ColaReparto();
            int opcion;
            do
            {
                Console.WriteLine("Que quieres hacer:");
                Console.WriteLine("1. Crear pedido.");
                Console.WriteLine("2. Mostrar próximo pedido.");
                Console.WriteLine("3. Entregar próximo pedido.");
                Console.WriteLine("0. Salir.");
                Console.Write("Opción: ");
                opcion = Int32.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Console.Write("Introduce el nombre del Cliente:");
                        string cliente = Console.ReadLine();
                        Console.Write("Introduce la Direccion: ");
                        string direccion = Console.ReadLine();
                        Console.Write("Introduce los platos (separados por comas): ");
                        string[] platos = Console.ReadLine().Split(",");
                        repartos.AgregarPedido(cliente,direccion,platos);
                        break;
                    case 2:
                        repartos.MostrarProximoPedido();
                        break;
                    case 3:
                        repartos.EliminarProximoPedido();
                        break;
                    case 0:
                        break;
                    default:
                        Console.WriteLine("Opción inválida.");
                        break;
                }
            } while (opcion != 0);

        }
    }
}
