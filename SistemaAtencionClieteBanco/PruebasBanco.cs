﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAtencionClieteBanco
{
    public class Solicitud
    {
        public string NombreCliente { get; set; }
        public string TipoSolicitud { get; set; }

        public Solicitud(string nombreCliente,string tipoSolicitud)
        {
            this.NombreCliente = nombreCliente;
            this.TipoSolicitud = tipoSolicitud;
        }
    }
    public class ColaCliente: Queue<Solicitud>
    {
        public int ColaQueue { get; set; }
        public Queue<Solicitud> cola;

        public ColaCliente()
        {
            cola = new Queue<Solicitud>();
        }
        public void AgregarSolicitud(string nombreCliente,string tipoSolicitud)
        {
            Solicitud solicitud = new Solicitud(nombreCliente, tipoSolicitud);
            cola.Enqueue(solicitud);
        }
        public void MostrarSiguienteSolicitud()
        {
            Solicitud solicitud = (Solicitud)cola.Peek();
            Console.WriteLine(solicitud.NombreCliente +", "+solicitud.TipoSolicitud);
        }
        public void EliminarSolicitudAtendida()
        {
            Solicitud solicitud = (Solicitud)cola.Peek();
            Console.WriteLine(solicitud.NombreCliente + ", " + solicitud.TipoSolicitud +" atendida");
            cola.Dequeue();
        }
        public void SolicitudesPendientes()
        {
            Console.WriteLine("Solicitudes pendientes: "+ cola.Count());
        }
    }
    class PruebasBanco
    {
        static void Main(string[] args)
        {
            string nombre;
            string motivo;
            string finSolicitud;
            ColaCliente solicitudes = new ColaCliente();
            do
            {
                Console.WriteLine("\nSistema de atención al cliente del Banco CaixaBank\n");
                Console.WriteLine("Introduzca el nombre");
                nombre = Console.ReadLine();
                Console.WriteLine("Introduzca el Tipo de Solicitud");
                //motivo = Console.ReadLine();
                motivo = "Deposito";
                solicitudes.AgregarSolicitud(nombre, motivo);
                solicitudes.MostrarSiguienteSolicitud();
                Console.WriteLine("Se ha solucionado su solicitud mostrada? (SI/NO)");
                finSolicitud = Console.ReadLine();
                if (finSolicitud.ToUpper() == "SI")
                {
                    solicitudes.EliminarSolicitudAtendida();
                }
                solicitudes.SolicitudesPendientes();
                Console.WriteLine("Escriba SALIR si quiere salir o pulse Enter si quiere continuar");
                nombre = Console.ReadLine();
            } while (nombre.ToUpper() != "SALIR" || nombre.ToUpper() != "SALIR");
        }
    }
}
