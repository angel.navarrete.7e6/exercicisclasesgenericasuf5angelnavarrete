﻿using System;
using System.Collections.Generic;

namespace SuperTorneoYuGiOh
{
    public class Carta
    {
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public int Nivel { get; set; }
        public string Efecto { get; set; }

        public Carta(string nombre, string tipo, int nivel, string efecto) {
            this.Nombre = nombre;
            this.Tipo = tipo;
            this.Nivel = nivel;
            this.Efecto = efecto;        
        }
    }
    public class MazoCartas : Stack<Carta>
    {
        Stack<Carta> pila;
        public MazoCartas()
        {
            pila = new Stack<Carta>();
        }
        public void AgregarCarta(Carta carta)
        {
            pila.Push(carta);
        } 
        public Carta RobarCarta()
        {
            if (EstaVacio()== false)
            {
                return pila.Pop();
            }
            else
            {
                Console.WriteLine("El Mazo esta vacío");
                return null;
            }
        }
        public bool EstaVacio()
        {
            if (pila.Count == 0) return true;
            else return false;
        }
    }
    public class ManoJugador 
    {
        List<Carta> cartas;
        public ManoJugador()
        {
            cartas = new List<Carta>();
        }

        public void JugarCarta(Carta carta)
        {
            Console.WriteLine("La carta "+carta.Nombre+ " usa: "+carta.Efecto);
            cartas.Remove(carta);
        }

        public void DescartarCarta(Carta carta)
        {
            cartas.Remove(carta);
        }

        public void RobarCarta(MazoCartas mazo)
        {
            Carta carta = mazo.RobarCarta();
            if (carta != null)
            {
                cartas.Add(carta);
            }
            else
            {
                Console.WriteLine("El mazo está vacío. ¡Has perdido!");
            }
        }

        public bool EstaVacio()
        {
            if (cartas.Count == 0) return true;
            else return false;
        }
    }
    class PruebasSuperTorneoYuGiOh
    {
        static void Main(string[] args)
        {
            MazoCartas mazo = new MazoCartas();
            mazo.AgregarCarta(new Carta("Mago Oscuro", "Monstruo", 3, "Lanzamiento de Conjuros"));
            mazo.AgregarCarta(new Carta("Dragón Blanco de Ojos Azules", "Monstruo", 2, "Dragon"));
            mazo.AgregarCarta(new Carta("Dragon Negro de Ojos Rojos", "Monstruo", 4, "Dragon"));
            mazo.AgregarCarta(new Carta("Parar La Defensa", "Hechizo", 4, "Se usa para la invocación por Ritual de 'Soldado del Brillo Negro' Tambien debes Sacrificar monstruos en tu mano o campo cuyos Niveles totales sean 8 o más."));
            mazo.AgregarCarta(new Carta("Guerreo Pantera", "Monstruo", 4, "Guerrero-Bestia"));
            mazo.AgregarCarta(new Carta("Tortuga Tacticas", "Monstruo", 5, "Aqua"));
            mazo.AgregarCarta(new Carta("Kuriboh", "Monstruo", 1, "Demonio"));
            mazo.AgregarCarta(new Carta("Fuerza de Espejo", "Trampa", 6, "Cuando un monstruo del adversario declara un ataque destruye todos los monstruos en Posicion de Ataque de tu adversario."));
            mazo.AgregarCarta(new Carta("Soborno Oscuro", "Trampa", 4, "Cuando tu adversario activa una Carta Magica de Trampa, tu adversario roba 1 carta y ademas niega la activacion de la Trampa. "));
            mazo.AgregarCarta(new Carta("Ira Divina", "Trampa", 4, "Cuando es activado el efecto de un monstruo: descarta 1 carta: niega la activacion y, si lo hace, destruye ese monstruo."));

            ManoJugador mano = new ManoJugador();

            while (!mazo.EstaVacio())
            {
                Carta carta = mazo.RobarCarta();
                if (carta != null)
                {
                    Console.WriteLine("¡Robaste la carta " + carta.Nombre + "!");
                    mano.JugarCarta(carta);
                }
            }

            if (!mano.EstaVacio())
            {
                Console.WriteLine("¡Ganaste el juego!");
            }
            else
            {
                Console.WriteLine("¡Perdiste el juego!");
            }

        }
    }
}
